import { Component,
         AfterViewInit }      from '@angular/core';
import { Response }           from '@angular/http';
import { Router }             from '@angular/router';
import { UserService }        from '../../service/user.service';

import { User }               from '../../model/user.model';

// external libraries references
declare var $: any;
declare var Materialize: any;

@Component({
    moduleId: module.id,
    providers: [
        UserService,
    ],
    selector: 'user',
    templateUrl: 'user.component.html'
})
export class UserComponent {

  user:User;
  pages: Array<number>;
  currentPage: number;
  totalPages: number;

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.getUsers(1);
  }

  private nextPage() {
    let nextPage:number = this.currentPage;
    nextPage++;
    this.getUsers(nextPage);
  }

  private prevPage() {
    let nextPage:number = this.currentPage;
    nextPage--;
    this.getUsers(nextPage);
  }

  private getUsers(page:number) {
    this.userService.getUsers(page)
      .subscribe(
        (user: User) => {
          this.user = user;
          this.totalPages = user.total_pages;
          this.currentPage = user.page;
          this.setNumberPages(user.total_pages)
      },
      (error: any) => {
        console.log(error);
      }
      ); 
  }

  private setNumberPages(numberPages:number) {
    this.pages = [];
    for (let i=1; i<=numberPages; i++) {
      this.pages.push(i);
    }
  }
}
