import { Injectable }            from '@angular/core';
import { Headers,
         Http,
         Response,
         RequestOptions,
         URLSearchParams }        from '@angular/http';
import { Observable }            from 'rxjs';
import { USER_LIST_CONFIG }     from '../shared/app/app.config';
import { User }                  from '../model/user.model';

import 'rxjs/add/operator/map'

@Injectable()
export class UserService {
    constructor(private http: Http) { }

    getUsers(page:number): Observable<User> {
      
      let params: URLSearchParams = new URLSearchParams();
      params.set('page', page.toString());

      return this.http.get(USER_LIST_CONFIG.apiEndpoints.user, {
        search: params
        }).map((response: Response) => {
          return response.json();
        });
    }
}
