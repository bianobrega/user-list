import { NgModule }                   from '@angular/core';
import { BrowserModule }              from '@angular/platform-browser';

import { RouterModule, Routes }       from '@angular/router';
import { AppComponent }               from './app.component';
import { HttpModule }                 from '@angular/http';

// Application components
import { PageNotFoundComponent }      from '../../components/page-not-found/page-not-found.component';
import { UserComponent }              from '../../components/user/user.component';
import { DefaultFooterComponent }     from './root-templates/footer/default-footer.component';
import { DefaultHeaderComponent }     from './root-templates/header/default-header.component';

// Pipes
import { LastNamePipe }                   from '../pipes/last-name.pipe';
import { FirstNamePipe }                   from '../pipes/first-name.pipe';

// Service to get user information data
import { APP_CONFIG, USER_LIST_CONFIG }  from './app.config';

import { Logger }                     from './logger.service';

// Routes
import { APP_ROUTES }                 from './app-routes.config';


@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(APP_ROUTES),
  ],
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    UserComponent,
    DefaultFooterComponent,
    DefaultHeaderComponent,
    LastNamePipe,
    FirstNamePipe,
  ],
  providers: [
    Logger,
    { provide: APP_CONFIG, useValue: USER_LIST_CONFIG }
  ],
  exports: [
    LastNamePipe,
    FirstNamePipe,
  ],

  bootstrap: [AppComponent],
})
export class AppModule { }
