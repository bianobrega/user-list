import { OpaqueToken } from '@angular/core';

export interface AppConfig {
  apiEndpoints: {'user': string};
  title: string;
}

export const USER_LIST_CONFIG: AppConfig = {
  apiEndpoints: {
    'user': 'https://reqres.in/api/users',
  },
  title: 'User List'
};
export let APP_CONFIG = new OpaqueToken('app.config');
