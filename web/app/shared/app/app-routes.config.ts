import { Routes }                     from '@angular/router';
import { PageNotFoundComponent }      from '../../components/page-not-found/page-not-found.component';
import { UserComponent }            from '../../components/user/user.component';

export const APP_ROUTES: Routes = [
    { path: 'usuarios', component: UserComponent, data: { title: 'Usuários' } },
    { path: '', component: UserComponent, data: { title: 'Usuários' } },
    { path: '**', component: PageNotFoundComponent }
];
