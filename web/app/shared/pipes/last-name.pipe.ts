import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'lastName'
})

export class LastNamePipe implements PipeTransform {
  transform(name: string): string {
     if (!name) {
       return '';
    }
           
    return name[0].toUpperCase().concat(".");
  }
   
}