import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'firstName'
})

export class FirstNamePipe implements PipeTransform {
  transform(name: string): string {
     if (!name) {
       return '';
    }

    var names = name.split(" ");
    var formatedName = "";

    for(var n of names) {
      formatedName += n.replace(/\w\S*/g, (txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase())).concat(" ");
    }
           
    return formatedName;
  }
   
}